#include "stdlib.h"
#include "GL/glut.h"
void Triangles(){
    glBegin(GL_TRIANGLES);
        glColor3f(0.5,0.5,0.5);
        glVertex2f (0.,0.);
        glVertex2f(10.,30.);
        glVertex2f(-10.,30.);
        glColor3f(0,0,0);
        glVertex2f (0.,0.);
        glVertex2f(-10.,-30.);
        glVertex2f(10.,-30.);
    glEnd();
}

void display(void){
    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(1,1,1,1);
    //glRotatef(3,0,0,1);//unclockwise
    glRotatef(3,0,0,-1); //clockwise
    Triangles();
    glFlush();
}

void timer(int value){
    glutPostRedisplay();
    glutTimerFunc(10,timer,0);
}

int main(int argc,char **argv){
    glutInit(&argc,argv);
    glutInitWindowPosition(100,100);
    glutInitWindowSize(400,400);
    glutCreateWindow("Baling-baling");
    gluOrtho2D(-50.,50.,-50.,50.);
    glutDisplayFunc(display);
    glutTimerFunc(1,timer,0);
    glutMainLoop();
}
