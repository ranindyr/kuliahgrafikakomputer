#include <GL/glut.h>
#include <stdlib.h>

void drawQuads(){
   glPushMatrix();
   glTranslatef(100, 50, 0);
   glRotated(30, 0, 0, 1);
   glBegin(GL_QUADS);
   glVertex2d(20,10);
   glVertex2d(10,-20);
   glVertex2d(-20,10);
   glVertex2d(10,20);
   glPopMatrix();

   glEnd();
   glFlush();
}

void drawQuads1(){
   glPushMatrix();
   glRotated(30, 0, 0, 1);
   glTranslatef(100, 50, 0);
   glBegin(GL_QUADS);
   glVertex2d(30,15);
   glVertex2d(15,-30);
   glVertex2d(-30,15);
   glVertex2d(15,30);
   glPopMatrix();

   glEnd();
   glFlush();
}

static void display(void)
{
    drawQuads();
    drawQuads1();
}

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);{

    glutInitDisplayMode(GLUT_DEPTH | GLUT_SINGLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(640,480);

    glutCreateWindow("Transformasi Translasi >x Rotasi Translasi");
    gluOrtho2D(-320.,320.,-240.,240.);
    glutDisplayFunc(display);
    glutMainLoop();

    return EXIT_SUCCESS;
    }
}
